import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import clean from 'gulp-clean';
import nunjucksRender from 'gulp-nunjucks-render';
import browserSyncPackage from 'browser-sync';
import postcss from 'gulp-postcss';
import atimport from 'postcss-import';
import stripCssComments from 'gulp-strip-css-comments';
import footer from 'gulp-footer';
import tailwindcss from 'tailwindcss';
import tailwindcssNesting from 'tailwindcss/nesting/index.js';
import autoprefixer from 'autoprefixer';
import sharpOptimizeImages from 'gulp-sharp-optimize-images';
import babel from 'gulp-babel';

import { series } from 'gulp';

const browserSync = browserSyncPackage.create();

/* This config file adds this project's files to be purged with PurgeCSS */
const TAILWIND_CONFIG = './config/tailwind.config.js';
/* This project's files to be compiled */
const SOURCE_HTML_DIR = './src/**.html';
/* This project's files AND desy files to be searched for in nunjucks recursive compilation */
const SOURCE_NUNJUCKS_PATHS = ['./src/templates/','./docs/','./node_modules/desy-html/src/templates/'];
/* This project's files to be watched */
const SOURCE_NUNJUCKS_FILES = ['./src/templates/**/*','./docs/**/*'];
/* This project's html files to be compiled */
const SOURCE_NUNJUCKS_DIR = ['./src/**/*.html','./docs/**/*.html'];

const SOURCE_JS_FILES = ['./node_modules/desy-html/src/js/**/*.js','!./node_modules/desy-html/src/js/globals/**','!./node_modules/desy-html/src/js/filters/**','./src/js/**/*.js'];
const SOURCE_STYLESHEET = './src/css/styles.css';
const SOURCE_STYLESHEET_DIR = './src/**/*.css';
const SOURCE_IMG_DIR = './src/img/**.*';
const DESTINATION_HTML_DIR = './dist/';
const DESTINATION_JS_DIR = './dist/js';
const DESTINATION_IMG_DIR = './dist/images';
const DESTINATION_STYLESHEET = './dist/css/';





function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  });

  cb();
}


function watchFiles(cb) {
  gulp.watch([TAILWIND_CONFIG, SOURCE_STYLESHEET_DIR, SOURCE_HTML_DIR, ...SOURCE_JS_FILES, ...SOURCE_NUNJUCKS_DIR, ...SOURCE_NUNJUCKS_FILES], gulp.series(html, nunjucks, js, css, reload));
  cb();
}


function reload(cb) {
  browserSync.reload();
  cb();
}


function css() {
  return gulp.src(SOURCE_STYLESHEET)
    .pipe(
      postcss([
      atimport(),
      tailwindcssNesting(),
      tailwindcss(TAILWIND_CONFIG),
      autoprefixer()
      ])
    )
    .pipe(stripCssComments({preserve: false}))
    .pipe(footer('\n'))
    .pipe(gulp.dest(DESTINATION_STYLESHEET));
}


function html() {
  return gulp.src(SOURCE_HTML_DIR)
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}


function img() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(sharpOptimizeImages({
      jpg_to_jpg: {
        quality: 80,
        mozjpeg: true,
      },
      png_to_png: {
        quality: 80,
        palette: true
      },
    }))
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}


function imgDev() {
  return gulp.src(SOURCE_IMG_DIR)
    .pipe(gulp.dest(DESTINATION_IMG_DIR));
}


function nunjucks() {
  return gulp.src(SOURCE_NUNJUCKS_DIR)
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true,
          noCache: true
        },
        path: SOURCE_NUNJUCKS_PATHS // String or Array
      }))
    .pipe(gulp.dest(DESTINATION_HTML_DIR));
}


function js() {
  return gulp.src(SOURCE_JS_FILES, { sourcemaps: true })
    .pipe(babel())
    .pipe(gulp.dest(DESTINATION_JS_DIR));
}


function version() {
  return gulp.src('./package.json')
    .pipe(gulp.dest('./dist/'));
}


function license() {
  return gulp.src('./src/EUPL-1.2.txt')
    .pipe(gulp.dest('./dist/'));
}


function cleanFolder() {
  return gulp.src(DESTINATION_HTML_DIR, {read: false, allowEmpty: true})
    .pipe(clean());
}

export default series(cleanFolder, html, imgDev, nunjucks, js, css, license, version, bs, watchFiles);
export const prod = series(cleanFolder, html, img, nunjucks, js, css, license, version, bs);